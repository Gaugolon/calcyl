import {Component, HostListener} from '@angular/core';
import {Keyboard, NavController, Platform, ToastController} from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  max_number: number = 100;
  min_number: number = 1;
  max_result: number = 100;
  a: number;
  b: number;
  result: number;
  input: number;
  fails: number;
  max_fails: number = 3;
  operation: Operation;
  operations: Operation[] = [
    {
      name: 'add',
      operator: '+',
      callback: this.getAddition,
    },
    {
      name: 'sub',
      operator: '-',
      callback: this.getSubtraction,
    }
  ];

  constructor(public navCtrl: NavController,
              private platform: Platform,
              private toastCtrl: ToastController,
              public keyboard: Keyboard) {
    this.next();
    document.addEventListener("deviceready", () => {
      console.log("console.log works well");
    }, false);
  }

  next() {
    this.result = null;
    this.input = null;
    this.fails = 0;
    this.setNumbers();
    this.keyboard.focusOutline({})
  }

  setNumbers() {
    this.operation = this.operations[Math.floor(Math.random() * this.operations.length)];
    switch (this.operation.name) {
      case 'add':
        this.getAddition();
        break;
      case 'sub':
        this.getSubtraction();
        break;
    }
  }

  getAddition() {
    this.result = Math.floor(Math.random() * this.max_result);
    this.a = Math.floor(Math.random() * this.result);
    this.b = this.result - this.a;
  }

  getSubtraction() {
    this.a = Math.floor(this.max_result * 0.5 + Math.random() * this.max_result * 0.5);
    this.b = Math.floor(this.a * Math.random());
    this.result = this.a - this.b;
  }

  check() {
    if (this.input == this.result || this.fails >= this.max_fails - 1) {
      this.next();
      this.onCorrect();
    } else {
      this.fails += 1;
      this.onFail();
    }
  }

  onFail() {
    let alert = this.toastCtrl.create({
      message: 'Nope',
      duration: 3000,
      position: 'top'
    });
    alert.present();
  }

  onCorrect() {
    let alert = this.toastCtrl.create({
      message: 'Jaaaaa....',
      duration: 3000,
      position: 'top'
    });
    alert.present();
  }

  @HostListener('document:keydown', ['$event']) handleKeyboardEvent(event: KeyboardEvent) {
    if (event.which === 9 || event.key == "Enter") {
      this.check();
      event.preventDefault();
    }
  }

}

export interface Operation {
  name: string;
  operator: string;

  callback(): void;
}
